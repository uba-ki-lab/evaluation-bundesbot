import json
from typing import Dict, List

import pandas as pd


def _convert_to_pd(dict_key: Dict, select_domains: List[str] = None):
    """Converting the dictionary to a pandas dataframe

    Sets the index to ['domain', 'slug'] and filters for elements in select_domains.
    If no column 'domain' exists (when loading the domains), 'slug' is renamed to 'domain'.

    Args:
        dict_key (Dict): part of the JSON-export parsed as dictionary
        select_domains (List[str], optional): domaines to filter for. Defaults to None.

    Returns:
        pandas.DataFrame: containing the elements of dict_key as rows
    """
    df = pd.DataFrame.from_dict(dict_key, orient="index").reset_index(drop=True)
    if not "domain" in df.columns:
        df = df.rename(columns={"slug": "domain"})
        df = df.set_index(["domain"]).sort_index()
    else:
        df = df.set_index(["domain", "slug"]).sort_index()
    if not select_domains:
        select_domains = set(df.index.get_level_values(0))
    available_domains = list(set(df.index.get_level_values(0)) & set(select_domains))
    return df.loc[available_domains]


def load_chatbot_domains(filename: str, select_domains: List[str] = None):
    """Loading Domains from the JSON-Export as pandas dataframe.

    Args:
        filename (str): path to JSON-Export
        select_domains (List[str], optional): domaines to filter for. Defaults to None.

    Returns:
        pandas.DataFrame: containing the domains as rows (index is domain slug, columns: label, description, order, folder, payload)

    Examples
    -----
    load_chatbot_domains(filename="../data/bot_configuration.json")
    load_chatbot_domains(filename="../data/bot_configuration.json", select_domains=["gesundheit"])
    """
    with open(filename, "r") as f:
        data = json.load(f)

    df_domains = _convert_to_pd(data["domains"], select_domains)
    return df_domains


def load_chatbot_questions(filename: str, select_domains: List[str] = None):
    """Loading Questions from the JSON-Export as pandas dataframe

    Args:
        filename (str): path to JSON-Export
        select_domains (List[str], optional): domaines to filter for. Defaults to None.

    Returns:
        pandas.DataFrame: containing the questions as rows (index is domain and question slug, columns: label, description, order, text)
    Examples
    -----
    load_chatbot_questions(filename="../data/bot_configuration.json")
    load_chatbot_questions(filename="../data/bot_configuration.json", select_domains=["gesundheit"])
    """

    with open(filename, "r") as f:
        data = json.load(f)

    df_intents = _convert_to_pd(data["intents"], select_domains)
    df_intents_samples = df_intents["samples"].explode().apply(pd.Series)[["text"]]
    df_intents = df_intents.drop("samples", axis=1)

    df_questions = df_intents.merge(
        df_intents_samples, right_index=True, left_index=True
    )

    return df_questions


def load_chatbot_answers(filename: str, select_domains: List[str] = None):
    """Loading Answers from the JSON-Export as pandas dataframe

    Args:
        filename (str): path to JSON-Export
        select_domains (List[str], optional): domaines to filter for. Defaults to None.

    Returns:
        pandas.DataFrame: containing the answers as rows (index is domain and answer slug, columns: label, description, type, channels, order, variable, value)
    Examples
    -----
    load_chatbot_answers(filename="../data/bot_configuration.json")
    load_chatbot_answers(filename="../data/bot_configuration.json", select_domains=["gesundheit"])
    """

    with open(filename, "r") as f:
        data = json.load(f)

    df_actions = _convert_to_pd(data["actions"], select_domains)
    df_actions_payload = df_actions["payload"].apply(pd.Series).melt(ignore_index=False)
    df_actions_payload = df_actions_payload[~df_actions_payload["value"].isna()]
    df_actions_payload = df_actions_payload.explode("value").sort_index()
    df_actions = df_actions.drop("payload", axis=1)

    df_answers = df_actions.merge(df_actions_payload, right_index=True, left_index=True)

    return df_answers


def load_chatbot_stories(filename: str, select_domains: List[str] = None):
    """Loading Stories from the JSON-Export as pandas dataframe.
    A story connects the questions to an answer.

    Args:
        filename (str): path to JSON-Export
        select_domains (List[str], optional): domaines to filter for. Defaults to None.

    Returns:
        pandas.DataFrame: containing the stories as rows (index is domain and question slug, columns: text, actionSlug, type, variable, value)
    Examples
    -----
    load_chatbot_stories(filename="../data/bot_configuration.json")
    load_chatbot_stories(filename="../data/bot_configuration.json", select_domains=["gesundheit"])
    """

    with open(filename, "r") as f:
        data = json.load(f)

    df_intents = _convert_to_pd(data["intents"], select_domains)
    df_intents_samples = df_intents["samples"].explode().apply(pd.Series)[["text"]]
    df_intents = df_intents.drop("samples", axis=1)

    df_actions = _convert_to_pd(data["actions"], select_domains)
    df_actions_payload = df_actions["payload"].apply(pd.Series).melt(ignore_index=False)
    df_actions_payload = df_actions_payload[~df_actions_payload["value"].isna()]
    df_actions_payload = df_actions_payload.explode("value").sort_index()
    df_actions = df_actions.drop("payload", axis=1)

    df_stories = _convert_to_pd(data["stories"], select_domains)
    df_storylines = df_stories["storyline"].explode().apply(pd.Series)
    # df_storylines["payload"].apply(pd.Series)["intentSlug"].dropna()
    df_storylines = (
        df_storylines["payload"].apply(pd.Series)["actionSlug"].dropna().to_frame()
    )
    df_stories = df_stories.drop("storyline", axis=1)

    df_intents_actions = (
        df_intents_samples.merge(df_storylines, left_index=True, right_index=True)
        .reset_index()
        .merge(
            df_actions.reset_index(1).rename({"slug": "actionSlug"}, axis=1)[
                ["actionSlug", "type"]
            ],
            left_on="actionSlug",
            right_on="actionSlug",
            how="left",
        )
        .merge(
            df_actions_payload.reset_index(1).rename({"slug": "actionSlug"}, axis=1),
            left_on="actionSlug",
            right_on="actionSlug",
            how="left",
        )
        .set_index(["domain", "slug"])
    )

    return df_intents_actions
