import re
from random import sample

import dspy


def _validate_distinct(existing_variations: list[str], result: str):
    """check if the result is distinct from previous variations"""

    if result.lower().strip() in [x.lower().strip() for x in existing_variations]:
        return False
    else:
        return True


def _validate_single_question(result: str):
    """check if the result contains exactly one question"""

    # dot matches all characters except the new line
    if re.match(r"^.+\?$", result):
        return True
    return False


# ------------------------------------------------------
# Generate Questions on a topic
# ------------------------------------------------------


class QuestionTopicSignature(dspy.Signature):
    """
    Generate a question that a person might ask to a Chatbot.
    The question must be written in German.
    The question must concentrate on the given topic.
    The question should be as short as possible.
    The newly generated question must be different from the already existing questions.
    """

    topic = dspy.InputField()
    existing_questions = dspy.InputField(desc="the already existing questions")
    new_question = dspy.OutputField()


class QuestionTopicModule(dspy.Module):
    """
    DSPy Module to create new questions on a topic.
    """

    def __init__(
        self,
        temperature=0.0,
    ):
        super().__init__()

        self.generateQuestionCoT = dspy.ChainOfThought(
            QuestionTopicSignature, temperature=temperature
        )

    def forward(self, topic: str, existing_questions: list[str] = []):

        existing_questions_str = "; ".join(
            f"{i+1}) {q}" for i, q in enumerate(existing_questions)
        )

        prediction = self.generateQuestionCoT(
            topic=topic,
            existing_questions=existing_questions_str,
        )

        new_question = prediction.new_question

        dspy.Assert(
            _validate_single_question(new_question),
            "Result should contain a single question",
        )

        dspy.Assert(
            _validate_distinct(existing_questions, new_question),
            "Result should be distinct from: " + existing_questions_str,
        )

        return new_question


class QuestionTopic:
    """
    Generating new questions on a given topic.

    Assertions of the DSPy module are activated.
    """

    def __init__(self, temperature=0.0, logger=None):
        super().__init__()

        self.qv = QuestionTopicModule(temperature=temperature).activate_assertions()

        self.logger = logger

    def generate(self, topic: str, existing_questions: list[str] = [], n=1):
        """Generate new questions.

        Args:
            topic (str): topic of the question
            existing_questions (list[str], optional): Questions that are checked against such that a new question is generated that does not already exist. Defaults to [].
            n (int, optional): Number of new questions. Defaults to 1.

        Returns:
            List[str]: list of new questions
        """
        new_questions = []

        for _ in range(n):
            try:
                new_question = self.qv(
                    topic=topic, existing_questions=existing_questions + new_questions
                )
                new_questions.append(new_question)
            except Exception as e:
                if self.logger:
                    self.logger.warn("For " + topic + ": %s", e)
                else:
                    print("For " + topic + ": " + str(e))

        return new_questions


# ------------------------------------------------------
# Generate Question Variations
# ------------------------------------------------------


class GenerateQuestionVariationSignature(dspy.Signature):
    """
    Generate a variation of how a person might ask the given question.
    The question must be written in German.
    The question should be as short as possible.
    The newly generated variation must be different from the already existing variations.
    """

    question = dspy.InputField(desc="the given question")
    existing_variations = dspy.InputField(
        desc="the already existing variations of the question"
    )
    new_variation = dspy.OutputField(
        desc="another way how a person might ask the given question"
    )


def _validate_no_prompt_new_variation(result: str):
    """check if the result does repeat the part of the prompt 'New Variation:'"""

    if re.search("new variation:", result.lower()):
        return False
    return True


class GenerateQuestionVariationModule(dspy.Module):
    """
    DSPy Module to create variations of a question.
    """

    def __init__(
        self,
        temperature=0.0,
    ):
        super().__init__()

        self.generateVariation = dspy.Predict(
            GenerateQuestionVariationSignature, temperature=temperature
        )

    def forward(self, question: str, existing_variations: list[str] = []):
        existing_variations_str = "; ".join(
            f"{i+1}) {q}" for i, q in enumerate(existing_variations)
        )
        prediction = self.generateVariation(
            question=question,
            existing_variations=existing_variations_str,
        )

        new_variation = prediction.new_variation

        dspy.Assert(
            _validate_single_question(new_variation),
            "Result should contain a single question",
        )

        dspy.Assert(
            _validate_distinct(existing_variations, new_variation),
            "Result should be distinct from: " + existing_variations_str,
        )

        dspy.Assert(
            _validate_no_prompt_new_variation(new_variation),
            "Result should not contain the string 'New Variation:'",
        )

        return new_variation


class GenerateQuestionVariation:
    """
    Generating variations of a question.

    Assertions of the DSPy module are activated.
    """

    def __init__(
        self,
        temperature=0.0,
        logger=None,
    ):
        self.qv = GenerateQuestionVariationModule(
            temperature=temperature
        ).activate_assertions()
        self.logger = logger

    def generate(self, questions: list[str], existing_variations: list[str] = [], n=1):
        """Generate varations of a question.
        If more than one question are given, one of them is randomly chosen.

        Args:
            questions (list[str]): list of questions
            existing_variations (list[str], optional): Questions that are checked against such that the variation does not already exist. Defaults to [].
            n (int, optional): Number of new variations. Defaults to 1.

        Returns:
            list[str]: list of new variations
        """

        new_variations = []
        for _ in range(n):
            try:
                random_question = sample(questions, 1)[0]
                existing_variations_list = (
                    existing_variations
                    + [x for x in questions if x != random_question]
                    + new_variations
                )

                new_variation = self.qv(
                    question=random_question,
                    existing_variations=existing_variations_list,
                )

                new_variations.append(new_variation.strip())
            except Exception as e:
                if self.logger:
                    self.logger.warn("For " + random_question + ": %s", e)
                else:
                    print("For " + random_question + ": " + str(e))

        return new_variations


# ------------------------------------------------------
# Generate Questions with mistakes
# ------------------------------------------------------


class GenerateMistakesSignature(dspy.Signature):
    """
    Generate a variation of the given text with incorrect spelling or grammar.
    The question must be written in German.
    The question should be as short as possible.
    The newly generated variation must be different from the already existing variations.
    """

    text = dspy.InputField(desc="the given text")
    existing_variations = dspy.InputField(
        desc="the already existing variations of the text"
    )
    text_with_mistakes = dspy.OutputField(desc="the text with mistakes")


def _validate_no_prompt_text_with_mistakes(result: str):
    """check if the result does not repeat the part of the prompt 'Text With Mistakes:'"""

    if re.search("text with mistakes:", result.lower()):
        return False
    return True


class GenerateMistakesModule(dspy.Module):
    """
    DSPy Module to create questions with typos.
    """

    def __init__(
        self,
        temperature=0.0,
    ):
        super().__init__()

        self.generateMistakes = dspy.Predict(
            GenerateMistakesSignature, temperature=temperature
        )

    def forward(self, text: str, existing_variations: list[str] = []):

        existing_variations_str = "; ".join(
            f"{i+1}) {q}" for i, q in enumerate(existing_variations)
        )

        prediction = self.generateMistakes(
            text=text,
            existing_variations=existing_variations_str,
        )

        text_with_mistakes = prediction.text_with_mistakes

        dspy.Assert(
            _validate_single_question(text_with_mistakes),
            "Result should contain a single question",
        )

        dspy.Assert(
            _validate_distinct(existing_variations, text_with_mistakes),
            "Result should be distinct from: " + existing_variations_str,
        )

        dspy.Assert(
            _validate_no_prompt_text_with_mistakes(text_with_mistakes),
            "Result should not contain the string 'Text With Mistakes:'",
        )

        return text_with_mistakes


class GenerateMistakes:
    """
    Generating questions with typos.

    Assertions of the DSPy module are activated.
    """

    def __init__(
        self,
        temperature=0.0,
        logger=None,
    ):
        self.logger = logger
        self.qv = GenerateMistakesModule(temperature=temperature).activate_assertions()

    def generate(self, text: str, existing_variations: list[str] = [], n=1):
        """Generate questions with typos.

        Args:
            text (str): the question
            existing_variations (list[str], optional): Questions that are checked against such that the variation does not already exist. Defaults to [].
            n (int, optional): Number of new variations. Defaults to 1.

        Returns:
            list[str]: list of new variations
        """
        new_variations = []
        for _ in range(n):
            try:
                text_with_mistakes = self.qv(
                    text=text,
                    existing_variations=existing_variations + new_variations + [text],
                )
                new_variations.append(text_with_mistakes)
            except Exception as e:
                if self.logger:
                    self.logger.warn("For " + text + ": %s", e)
                else:
                    print("For " + text + ": " + str(e))

        return new_variations


# ------------------------------------------------------
# Generate Translations
# ------------------------------------------------------


def _validate_no_prompt_language(result: str, language: str):
    """check if the result does not repeat the part of the prompt 'Language: <language>'"""

    if re.search("language: " + language.lower(), result.lower()):
        return False
    return True


class QuestionTranslateSignature(dspy.Signature):
    """
    Translate the text into the given language.
    """

    text = dspy.InputField(desc="the text")
    language = dspy.InputField(desc="the language")
    translation = dspy.OutputField(desc="the translation")


class QuestionTranslateModule(dspy.Module):
    """
    DSPy Module to translate a question.
    """

    def __init__(
        self,
        temperature=0.0,
    ):
        super().__init__()

        self.translate = dspy.Predict(
            QuestionTranslateSignature, temperature=temperature
        )

    def forward(self, text: str, language: str):

        result = self.translate(text=text, language=language).translation

        dspy.Assert(
            _validate_single_question(result),
            "Result should contain a single question",
        )

        dspy.Assert(
            _validate_no_prompt_language(result, language),
            "Result should not contain the string 'Language: " + language + "'",
        )

        return result


class QuestionTranslate:
    """
    Translate a question.

    Assertions of the DSPy module are activated.
    """

    def __init__(self, temperature=0.0, logger=None):
        super().__init__()

        self.qt = QuestionTranslateModule(temperature=temperature).activate_assertions()
        self.logger = logger

    def generate(self, text: str, language: str):
        """Translate the question into the given language.

        Args:
            text (str): the question
            language (str): the language

        Returns:
            str: the translation
        """
        try:
            return self.qt(
                text=text,
                language=language,
            )
        except Exception as e:
            if self.logger:
                self.logger.warn("For " + text + " (" + language + "): %s", e)
            else:
                print("For " + text + " (" + language + "): " + str(e))
            return ""
